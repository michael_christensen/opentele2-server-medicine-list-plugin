package org.opentele.server.citizen.api.medicineList

import grails.buildtestdata.mixin.Build
import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.opentele.server.citizen.medicinelist.MedicineListService
import org.opentele.server.model.*
import spock.lang.Specification

@TestFor(MedicineListController)
@Mock([Patient])
@Build([Patient, Document])
class MedicineListControllerSpec extends Specification {

    def mockMedicineListService

    def setup() {
        mockMedicineListService = Mock(MedicineListService)
        controller.medicineListService = mockMedicineListService
    }

    def "can get medicine list for patient"() {
        given:
        setupMedicineListForPatient()

        when:
        controller.show()

        then:
        response.status == 200
        response.contentType == 'application/pdf'
        response.contentAsByteArray.size() == 8
        new String(response.contentAsByteArray, 'UTF-8') == 'test doc'
    }

    def "if patient has no medicine list 404 returned"() {
        when:
        def reply = controller.show()

        then:
        reply.status == 404
        reply.resourceType == 'errors'
        reply.resource.message.contains('not found')
    }

    def "document marked as read when served"() {
        given:
        def document = setupMedicineListForPatient()

        when:
        controller.show()

        then:
        1 * mockMedicineListService.markAsRead(document)
    }

    def "can get summary description of medicineList"() {
        given:
        def document = setupMedicineListForPatient()

        when:
        def reply = controller.summary()

        then:
        reply.resourceType == 'medicineListSummary'
        def resource = reply.resource
        resource.uploadDate == document.uploadDate
        resource.isNew == !document.isRead
        resource.links.medicineList != null
        resource.links.self != null
    }

    def "if patient has no existing medicine list summary 404 is returned"() {
        when:
        def reply = controller.summary()

        then:
        reply.status == 404
        reply.resourceType == 'errors'
        reply.resource.message.contains('not found')
    }

    private def setupMedicineListForPatient() {
        def bytes = "test doc".getBytes()
        def doc = Document.build(category: DocumentCategory.MEDICINE_LIST, content: bytes, fileType: FileType.PDF)
        mockMedicineListService.getForPatient() >> doc
        doc
    }
}
