class MedicineListUrlMappings {

	static mappings = {
        name medicineList: "/patient/medicinelist"(parseRequest:true) {
            controller = 'medicineList'
            action = 'show'
        }

        name medicineListSummary: "/patient/medicineListSummary"(parseRequest: true) {
            controller = 'medicineList'
            action = 'summary'
        }
	}
}
