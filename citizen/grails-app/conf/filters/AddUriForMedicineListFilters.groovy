package filters

import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib

class AddUriForMedicineListFilters {

    def responsePipelineService
    def medicineListService

    def filters = {
        all(controller:'*', action:'*') {
            before = {
                responsePipelineService.registerResponseListener {type, resource ->
                    if (type != 'patient') {
                        return
                    }

                    if (medicineListService.existsForPatient()) {
                        resource.links.medicineListSummary = new ApplicationTagLib().createLink(mapping: 'medicineListSummary', absolute: true)
                    }
                }
            }
        }
    }
}
