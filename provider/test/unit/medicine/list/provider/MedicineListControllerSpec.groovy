package medicine.list.provider

import grails.buildtestdata.mixin.Build
import grails.test.mixin.*
import org.codehaus.groovy.grails.plugins.testing.GrailsMockMultipartFile
import org.opentele.server.medicinelist.MedicineListController
import org.opentele.server.model.Document
import org.opentele.server.model.DocumentCategory
import org.opentele.server.model.FileType
import org.opentele.server.model.Patient
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import spock.lang.Specification

import java.text.SimpleDateFormat

@TestFor(MedicineListController)
@Build([Patient, Document])
@Mock([Patient, Document])
class MedicineListControllerSpec extends Specification {

    Patient patient

    def setup() {
        patient = Patient.build().save(failOnError: true)
        controller.shouldConvertPdf = false
    }

    def "can show summary info for existing medicine list"() {
        given:
        def document = Document.build(patient: patient, category: DocumentCategory.MEDICINE_LIST).save(failOnError: true)

        when:
        params.id = patient.id.toString()
        def viewModel = controller.show()

        then:
        viewModel.document == document
        viewModel.patientId == patient.id
    }

    def "can show empty summary for patient without existing medicine list"() {
        when:
        params.id = patient.id.toString()
        def viewModel = controller.show()

        then:
        viewModel.document.id == null
        viewModel.patientId == patient.id
    }

    def "can open existing medicine list for viewing"() {
        given:
        def document = Document.build().save(failOnError: true)

        when:
        params.id = document.id.toString()
        controller.open()

        then:
        response.contentType == 'application/pdf'
        response.getHeader('Content-disposition').contains('inline')
    }

    def "can upload new medicine list that replaces existing"() {
        given:
        def document = Document.build(isRead: true, patient: patient, category: DocumentCategory.MEDICINE_LIST).save(failOnError: true)

        when:
        params.id = patient.id.toString()
        request.addFile(createMockFile())
        controller.upload()

        then:
        def updated = Document.get(document.id)
        assertCorrectDocument(updated)
        response.redirectedUrl == "/medicineList/show/${patient.id}"
    }

    def "can upload medicine list for patient without existing"() {
        when:
        params.id = patient.id.toString()
        request.addFile(createMockFile())
        controller.upload()

        then:
        def updated = Document.findByPatient(patient)
        assertCorrectDocument(updated)
        response.redirectedUrl == "/medicineList/show/${patient.id}"
    }

    def "uploading a non-pdf file will fail"() {
        given:
        def invalidFile = createMockFile()
        invalidFile.contentType = 'image/png'

        when:
        params.id = patient.id.toString()
        request.addFile(invalidFile)
        controller.upload()

        then:
        flash.message.contains('content.type.not.valid')
        response.redirectedUrl == "/medicineList/show/${patient.id}"
    }

    def "pressing upload without a file selected will show message"() {
        given:
        def noFile = createMockFile('wrong_name_will_not_be_found')

        when:
        params.id = patient.id.toString()
        request.addFile(noFile)
        controller.upload()

        then:
        flash.message.contains('no.file.selected')
        response.redirectedUrl == "/medicineList/show/${patient.id}"
    }

    private assertCorrectDocument(Document updated) {
        updated.filename == 'IronMan3.pdf'
        updated.fileType.toString() == 'application/pdf'
        updated.fileType == FileType.PDF
        updated.content == "1234567" as byte[]
        updated.isRead == false
        updated.category == DocumentCategory.MEDICINE_LIST
        def precisionSeconds = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        precisionSeconds.format(updated.uploadDate) == precisionSeconds.format(new Date())
    }

    private createMockFile(name) {
        if (!name) {
            name = 'file'
        }
        new MockMultipartFile(name, 'IronMan3.pdf', 'application/pdf', "1234567" as byte[])
    }
}
