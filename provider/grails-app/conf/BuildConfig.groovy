grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        grailsCentral()
        mavenCentral()
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        // runtime 'mysql:mysql-connector-java:5.1.27'
    }

    plugins {
        compile ':spring-security-core:2.0-RC4'
        build(":tomcat:7.0.54",
                ":release:3.0.1",
                ":rest-client-builder:2.0.3") {
            export = false
        }
        runtime ":hibernate4:4.3.5.5"
    }
}
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        grailsCentral()
        mavenCentral()
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        // runtime 'mysql:mysql-connector-java:5.1.27'
    }

    plugins {
        compile ':spring-security-core:2.0-RC4'
        build(":tomcat:7.0.54",
                ":release:3.0.1",
                ":rest-client-builder:2.0.3") {
            export = false
        }
    }
}


def corePluginDirectory = new File('../opentele-server-core-plugin')
if (!corePluginDirectory.exists()) {
    corePluginDirectory = new File('../../opentele-server-core-plugin')
}
if (corePluginDirectory.exists()) {
    grails.plugin.location.'OpenteleServerCorePlugin' = corePluginDirectory.absolutePath
}
else {
    println("ERROR: Unable to find core plugin!")
}
