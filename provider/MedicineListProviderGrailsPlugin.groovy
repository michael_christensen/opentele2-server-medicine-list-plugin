class MedicineListProviderGrailsPlugin {
    def version = "1.0"
    def grailsVersion = "2.4 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    def title = "Provider Server Medicine List Plugin"
    def author = "Silverbullet A/S"
    def authorEmail = ""
    def description = '''\
Plugin for provider server to serve patient medicine lists for clients.
'''

    def license = "APACHE"
    def organization = [ name: "4S", url: "4s-online.dk" ]
    def developers = [ [ name: "Silverbullet A/S", email: "TeleCare-support@silverbullet.dk" ] ]

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before
    }

    def doWithSpring = {
        // TODO Implement runtime spring config (optional)
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { ctx ->
        // TODO Implement post initialization spring config (optional)
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }

    def onShutdown = { event ->
        // TODO Implement code that is executed when the application shuts down (optional)
    }
}
